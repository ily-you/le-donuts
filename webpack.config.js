const Debpack = require('./build/debpack');

Debpack
    .addScript('scripts', './scripts/index.ts')
    .addStyle('styles', './styles/index.scss')
    .configFavicon('./assets/images/favicon.png')
;

module.exports = Debpack.getWebpackConfig();
